import { assert } from 'chai';
import 'mocha';
import axios from 'axios';
import * as k8s from '@kubernetes/client-node';
import { cleanup } from '../subtest/cleanup';
// import { snapshot } from '../subtest/createSnapshot';
import { workflow } from '../subtest/runWorkflow';
import { BASEURL, NAMESPACE, delay } from '../utils/globals';
import {
  GetNotebookResponse,
  PostNotebookResponse,
  UserRequestedLibrary,
} from '../utils/types';

const userRequestedLibraries: UserRequestedLibrary[] = [
  {
    libname: 'prov',
    libversion: '1.5.2',
    source: null,
  },
];

describe('Create notebook', function () {
  let notebookResponse: PostNotebookResponse;
  let kc: k8s.KubeConfig;

  let pod: k8s.V1Pod | undefined;

  before(async () => {
    kc = new k8s.KubeConfig();
    kc.loadFromDefault();

    notebookResponse = (
      await axios.post(
        `${BASEURL}/notebook`,
        { userRequestedLibraries },
        {
          headers: {
            accept: '*/*',
            'Content-Type': 'application/json',
          },
        }
      )
    ).data as PostNotebookResponse;
    if (notebookResponse.serviceURL.includes('rdwd.dev.knmi.cloud')) {
      notebookResponse.serviceURL = `http://jupyter-${
        notebookResponse.serviceURL.split('/swirrl/jupyter/')[1].split('/')[0]
      }${notebookResponse.serviceURL.split('rdwd.dev.knmi.cloud')[1]}`;
    }
  });

  it('should return id', () =>
    assert.match(
      notebookResponse.id,
      /^\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b$/
    ));
  it('should return poolId', () =>
    assert.match(
      notebookResponse.poolId,
      /^\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b$/
    ));
  it('should return sessionId', () =>
    assert.match(
      notebookResponse.sessionId,
      /^\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b$/
    ));
  it('should return correct requested libraries', () =>
    assert.deepEqual(
      notebookResponse.userRequestedLibraries,
      userRequestedLibraries
    ));

  it('should become available', async () => {
    const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
    for (let i = 0; i < 1200; i += 5) {
      const deploymentResponse = await k8sApi.readNamespacedPod(
        `jupyter-${notebookResponse.id}`,
        NAMESPACE
      );
      const status = deploymentResponse.body.status;
      if (status && status.phase === 'Running') {
        return;
      }
      await delay(5000);
    }
    throw Error('Deployment did not become available');
  }).timeout(1250000);

  it('should have running pod', async () => {
    const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
    const podResponse = await k8sApi.listNamespacedPod(NAMESPACE);
    pod = podResponse.body.items.find(
      (pod) =>
        pod &&
        pod.metadata &&
        pod.metadata.labels &&
        pod.metadata.labels['service-id'] === notebookResponse.id
    );
    assert.equal(pod && pod.status && pod.status.phase, 'Running');
  });

  it('should have service', async () => {
    const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
    const service = await k8sApi.readNamespacedService(
      `jupyter-${notebookResponse.id}`,
      NAMESPACE
    );
    assert.exists(service.body);
  });

  it('should have all volumes', () => {
    if (pod && pod.spec && pod.spec.volumes) {
      assert.exists(pod.spec.volumes.find((volume) => volume.name === 'data'));
      assert.exists(
        pod.spec.volumes.find((volume) => volume.name === 'working-directory')
      );
      assert.exists(
        pod.spec.volumes.find((volume) => volume.name === 'startup-directory')
      );
      assert.exists(
        pod.spec.volumes.find((volume) => volume.name === 'pip-wrapper')
      );
      assert.exists(
        pod.spec.volumes.find((volume) => volume.name === 'conda-wrapper')
      );
      assert.exists(
        pod.spec.volumes.find((volume) => volume.name === 'env-var')
      );
    } else {
      throw Error('No volumes found');
    }
  });

  it('should have ingress rule', async () => {
    const k8sApi = kc.makeApiClient(k8s.NetworkingV1Api);
    if (pod && pod.spec) {
      const ingress = await k8sApi.readNamespacedIngress(
        'swirrl-services',
        NAMESPACE
      );
      if (
        ingress.body.spec &&
        ingress.body.spec.rules &&
        ingress.body.spec.rules[0].http
      ) {
        const paths = ingress.body.spec.rules[0].http.paths;
        assert.exists(
          paths.find(
            (path) =>
              path.backend.service?.name === `jupyter-${notebookResponse.id}`
          )
        );
      } else {
        throw Error('No ingress found');
      }
    } else {
      throw Error('Pod does not have spec');
    }
  });

  it('should be reachable', async () => {
    for (let i = 0; i < 30; i += 5) {
      try {
        const response = await axios.get(notebookResponse.serviceURL);
        assert.equal(response.status, 200);
        return;
      } catch (err) {
        await delay(5000);
      }
    }
    throw Error('Notebook not reachable');
  }).timeout(35000);

  it('should have correct installed libraries', async () => {
    const response = (
      await axios.get(`${BASEURL}/notebook/${notebookResponse.id}`)
    ).data as GetNotebookResponse;
    userRequestedLibraries.forEach((requestedLib) => {
      const installedLib = response.installedLibraries.find(
        (lib: UserRequestedLibrary) => lib.libname === requestedLib.libname
      );
      assert.exists(installedLib);
      if (requestedLib.libversion && installedLib) {
        assert.include(installedLib.libversion, requestedLib.libversion);
      }
    });
  });

  after(() => {
    // snapshot(notebookResponse);
    workflow(notebookResponse);
    cleanup(this.title, notebookResponse);
  });
});
