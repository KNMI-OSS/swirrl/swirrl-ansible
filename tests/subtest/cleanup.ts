/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import 'mocha';
import axios from 'axios';
import * as k8s from '@kubernetes/client-node';
import { BASEURL, NAMESPACE, delay } from '../utils/globals';
import { PostNotebookResponse } from '../utils/types';

export const cleanup = (
  title: string,
  notebookResponse: PostNotebookResponse
): Mocha.Suite =>
  describe(`Cleanup for "${title}"`, () => {
    let kc: k8s.KubeConfig;
    let notebookPod: k8s.V1Pod | undefined;
    let snapshotPod: k8s.V1Pod | undefined;

    before(async () => {
      kc = new k8s.KubeConfig();
      kc.loadFromDefault();
      const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
      const notebookPodResponse = await k8sApi.listNamespacedPod(NAMESPACE);
      notebookPod = notebookPodResponse.body.items.find(
        (pod) =>
          pod &&
          pod.metadata &&
          pod.metadata.labels &&
          pod.metadata.labels['service-id'] === notebookResponse.id
      );
      const snapshotPodResponse = await k8sApi.listNamespacedPod(NAMESPACE);
      snapshotPod = snapshotPodResponse.body.items.find(
        (pod) =>
          pod &&
          pod.metadata &&
          pod.metadata.labels &&
          pod.metadata.labels['cssservice-id'] === notebookResponse.id
      );
    });

    describe('Delete notebook', () => {
      before(async () => {
        await axios.delete(`${BASEURL}/notebook/${notebookResponse.id}`, {
          headers: {
            accept: '*/*',
          },
        });
      });

      it('should delete deployment', async () => {
        const k8sApi = kc.makeApiClient(k8s.AppsV1Api);
        for (let i = 0; i < 150; i += 5) {
          try {
            await k8sApi.readNamespacedDeployment(
              `jupyter-${notebookResponse.id}`,
              NAMESPACE
            );
          } catch (err: any) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            assert.equal(err.statusCode, 404);
            return;
          }
          await delay(5000);
        }
        throw Error('Deployment did not get deleted');
      }).timeout(170000);

      it('should delete notebook pod', async () => {
        if (notebookPod && notebookPod.metadata && notebookPod.metadata.name) {
          const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
          for (let i = 0; i < 150; i += 5) {
            try {
              await k8sApi.readNamespacedPod(
                notebookPod.metadata.name,
                NAMESPACE
              );
            } catch (err: any) {
              // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
              assert.equal(err.statusCode, 404);
              return;
            }
            await delay(5000);
          }
          throw Error('Pod did not get deleted');
        } else {
          throw Error('Pod does not have name');
        }
      }).timeout(170000);

      it('should delete service', async () => {
        const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
        try {
          await k8sApi.readNamespacedService(
            `jupyter-${notebookResponse.id}`,
            NAMESPACE
          );
        } catch (err: any) {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          assert.equal(err.statusCode, 404);
        }
      });

      it('should delete all configmaps', async () => {
        const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
        let configMapList: k8s.V1ConfigMap[] = [];
        try {
          configMapList = (await k8sApi.listNamespacedConfigMap(NAMESPACE)).body
            .items;
        } catch (err: any) {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          assert.equal(err.statusCode, 404);
          return;
        }

        assert(
          !configMapList.some(
            (configMap) =>
              configMap.metadata &&
              configMap.metadata.labels &&
              configMap.metadata.labels['service-id'] === notebookResponse.id
          )
        );
      });

      it('should remove ingress rule', async () => {
        const k8sApi = kc.makeApiClient(k8s.NetworkingV1Api);
        const ingress = await k8sApi.readNamespacedIngress(
          'swirrl-services',
          NAMESPACE
        );
        if (
          ingress.body.spec &&
          ingress.body.spec.rules &&
          ingress.body.spec.rules[0].http
        ) {
          const paths = ingress.body.spec.rules[0].http.paths;
          assert.notExists(
            paths.find(
              (path) =>
                path.backend.service?.name === `jupyter-${notebookResponse.id}`
            )
          );
        } else {
          throw Error('No ingress found');
        }
      });
    });

    describe('Delete session', () => {
      before(async () => {
        await axios.delete(`${BASEURL}/session/${notebookResponse.sessionId}`, {
          headers: {
            accept: '*/*',
          },
        });
      });

      it("should delete all pvc's", async () => {
        const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
        for (let i = 0; i < 100; i += 5) {
          let pvcList: k8s.V1PersistentVolumeClaim[] = [];
          try {
            pvcList = (
              await k8sApi.listNamespacedPersistentVolumeClaim(NAMESPACE)
            ).body.items;
          } catch (err: any) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            assert.equal(err.statusCode, 404);
            return;
          }

          if (
            !pvcList.some(
              (pvc) =>
                pvc.metadata &&
                pvc.metadata.labels &&
                pvc.metadata.labels['session-id'] === notebookResponse.sessionId
            )
          ) {
            return;
          }
          await delay(5000);
        }
        throw Error("pvc's did not get deleted");
      }).timeout(150000);

      it('should delete snapshot pod', async () => {
        if (!snapshotPod) {
          return true;
        }
        if (snapshotPod && snapshotPod.metadata && snapshotPod.metadata.name) {
          const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
          for (let i = 0; i < 50; i += 5) {
            try {
              await k8sApi.readNamespacedPod(
                snapshotPod.metadata.name,
                NAMESPACE
              );
            } catch (err: any) {
              // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
              assert.equal(err.statusCode, 404);
              return;
            }
            await delay(5000);
          }
          throw Error('Pod did not get deleted');
        } else {
          throw Error('Pod does not have name');
        }
      }).timeout(100000);
    });
  });
