import 'mocha';
import axios from 'axios';
import { HOSTNAME, delay } from '../utils/globals';

export async function mochaGlobalSetup(): Promise<void> {
  for (let i = 0; i < 300; i += 5) {
    const response = await axios
      .get(`${HOSTNAME}/swirrl-api`)
      .catch((err: { code: number }) =>
        console.log(`Swirrl API is not yet available: ${err.code}`)
      );
    if (response && response.status === 200) {
      return;
    }
    await delay(5000);
  }
  throw Error('Swirrl API did not become available');
}
