import { assert } from 'chai';
import 'mocha';
import axios, { AxiosError } from 'axios';
import { BASEURL, delay } from '../utils/globals';
import {
  GetNotebookResponse,
  PostNotebookResponse,
  ACTIVITIES,
} from '../utils/types';
import { findActivityIdByType, getActivityById } from '../utils/getActivities';

type Job = {
  id: string;
  status: string;
  logs: string;
};

type FileList = {
  name: string;
  path: string;
  content: string;
};

export const workflow = (notebookResponse: PostNotebookResponse): Mocha.Suite =>
  describe('Create workflow', () => {
    let workflowResponse: Job;
    let getStatusResponse: Job;

    it('should have status "Workflow in progress"', async () => {
      workflowResponse = (
        await axios.post(
          `${BASEURL}/workflow/download/run/`,
          {
            sessionId: notebookResponse.sessionId,
            inputs:
              'bmFtZTogJ1Rlc3QgZG93bmxvYWQgd29ya2Zsb3cnCm1lc3NhZ2U6ICdEb3dubG9hZCBmaWxlcyBm' +
              'cm9tIGhpZ2hseSBhdmFpbGFibGUgY2RuanMgd2Vic2l0ZScKCmxpbmtzOgogIC0gdXJsOiAnaHR0' +
              'cHM6Ly9jZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvc3dhZ2dlci11aS80LjEuMi9zd2Fn' +
              'Z2VyLXVpLmpzJwogICAgZmlsZW5hbWU6IGZpbGVuYW1lMS5qcwogIC0gdXJsOiAnaHR0cHM6Ly9j' +
              'ZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvbW9jaGEvOS4xLjMvbW9jaGEubWluLmpzJwog' +
              'ICAgZmlsZW5hbWU6IGZpbGVuYW1lMi5qcwogIC0gdXJsOiAnaHR0cHM6Ly9jZG5qcy5jbG91ZGZs' +
              'YXJlLmNvbS9hamF4L2xpYnMvcmVhY3QvMTcuMC4yL3VtZC9yZWFjdC5wcm9kdWN0aW9uLm1pbi5q' +
              'cycKICAgIGZpbGVuYW1lOiBmaWxlbmFtZTMuanMKCmZpbGVvdXQ6ICd0ZXN0Jwo=',
          },
          {
            headers: {
              accept: '*/*',
              'Content-Type': 'application/json',
            },
          }
        )
      ).data as Job;
      let getResponse = { notebookStatus: 'None' };
      for (let i = 0; i < 1200; i += 5) {
        getResponse = (
          await axios.get(`${BASEURL}/notebook/${notebookResponse.id}`)
        ).data as GetNotebookResponse;
        if (getResponse.notebookStatus === 'Workflow in progress') return;
        await delay(5000);
      }
      throw Error(
        `Workflow status did not change to in progress. Status = ${getResponse.notebookStatus}`
      );
    }).timeout(600000);

    it('create snapshot should return error', async () => {
      await axios
        .post(
          `${BASEURL}/notebook/${notebookResponse.id}/snapshot`,
          {
            userMessage: 'Should fail.',
          },
          {
            headers: {
              accept: '*/*',
              'Content-Type': 'application/json',
            },
          }
        )
        .catch((error: AxiosError) => {
          if (error.response) {
            assert.equal(error.response.status, 500);
            const errorData = error.response.data as { description: string };
            assert.match(
              errorData.description,
              /Impossible to run operation on notebook due to active job\(s\):.*/
            );
          } else {
            throw error;
          }
        });
    });

    it('delete latest stage should return error', async () => {
      await axios
        .delete(
          `${BASEURL}/workflow/${notebookResponse.sessionId}/deletelatest`
        )
        .catch((error: AxiosError) => {
          if (error.response) {
            assert.equal(error.response.status, 400);
          } else {
            throw error;
          }
        });
    });

    it('Waiting for workflow to complete', async () => {
      for (let i = 0; i < 1200; i += 5) {
        getStatusResponse = (
          await axios.get(
            `${BASEURL}/workflow/download/run/${workflowResponse.id}/`
          )
        ).data as Job;
        if (!['Pending', 'Running'].includes(getStatusResponse.status)) return;
        await delay(5000);
      }
      throw Error(
        `Workflow failed or did not complete. Status = ${getStatusResponse.status}`
      );
    }).timeout(1250000);

    it('should return to status "Active"', async () => {
      const getResponse = (
        await axios.get(`${BASEURL}/notebook/${notebookResponse.id}`)
      ).data as GetNotebookResponse;
      assert.equal(getResponse.notebookStatus, 'Active');
    });

    it('should have status "Succeeded"', async () => {
      getStatusResponse = (
        await axios.get(
          `${BASEURL}/workflow/download/run/${workflowResponse.id}/`
        )
      ).data as Job;
      assert.equal(getStatusResponse.status, 'Succeeded');
    });

    const svcUrlToken = notebookResponse.serviceURL.split('?');
    it('should have stored data', async () => {
      const listFiles = (
        await axios.get(
          `${svcUrlToken[0]}/api/contents/data/staginghistory/stage.00001/?${svcUrlToken[1]}`
        )
      ).data as FileList;
      assert.hasAnyKeys(listFiles, ['name', 'path', 'content']);
      assert.equal(listFiles.name, 'stage.00001');
      assert.isArray(listFiles.content);
      assert.lengthOf(listFiles.content, 4);
    });

    it('should have created a swirrl info file', async () => {
      const swirrlInfoFile = await axios.get(
        `${svcUrlToken[0]}/api/contents/data/staginghistory/stage.00001/swirrl_fileinfo.json?${svcUrlToken[1]}`
      );
      assert.equal(swirrlInfoFile.status, 200);
    });

    it('should generate provenance for the workflow activity', async () => {
      let activityList = null;
      for (let i = 0; i < 120; i += 5) {
        try {
          activityList = await findActivityIdByType(
            notebookResponse.sessionId,
            ACTIVITIES.WORKFLOW
          );
          assert.isNotEmpty(activityList);
          break;
        } catch (err) {
          await delay(5000);
        }
      }
      if (activityList === null) {
        throw Error('No workflow activity was made');
      }
      const activityResponse = await getActivityById(activityList[0]['@id']);
      assert.isNotEmpty(activityResponse);
    }).timeout(125000);
  });
