/* eslint-disable camelcase */
import { assert } from 'chai';
import 'mocha';
import axios from 'axios';
import YAML from 'yaml';
import * as k8s from '@kubernetes/client-node';
import { BASEURL, NAMESPACE, GITHUBAPI, delay } from '../utils/globals';
import {
  GetNotebookResponse,
  PostNotebookResponse,
  ACTIVITIES,
} from '../utils/types';
import { findActivityIdByType, getActivityById } from '../utils/getActivities';

type SnapshotResponse = {
  snapshotURL: string;
};

type Contents = {
  name: string;
  path: string;
  sha: string;
  size: number;
  url: string;
  html_url: string;
  git_url: string;
  download_url: string;
  type: string;
  _links: Record<string, string>;
}[];

type Pip = {
  pip: string[];
};

type Environment = {
  name: string;
  channels: string[];
  prefix: string;
  dependencies: (string | Pip)[];
};

const expectedContents = [
  {
    name: 'download_data.sh',
    type: 'file',
  },
  {
    name: 'environment.yml',
    type: 'file',
  },
  {
    name: 'postBuild',
    type: 'file',
  },
  {
    name: 'swirrl-docker',
    type: 'dir',
  },
];

export const snapshot = (notebookResponse: PostNotebookResponse): Mocha.Suite =>
  describe('Create snapshot', () => {
    let snapshotResponse: SnapshotResponse;
    let kc: k8s.KubeConfig;

    let pod: k8s.V1Pod | undefined;
    let contents: Contents;

    before(() => {
      kc = new k8s.KubeConfig();
      kc.loadFromDefault();
    });

    it('should have status "Snapshot in progress"', async () => {
      const snapshotPromise = axios.post(
        `${BASEURL}/notebook/${notebookResponse.id}/snapshot`,
        {
          userMessage: 'Created by smoke test.',
        },
        {
          headers: {
            accept: '*/*',
            'Content-Type': 'application/json',
          },
        }
      );
      const getResponse = (
        await axios.get(`${BASEURL}/notebook/${notebookResponse.id}`)
      ).data as GetNotebookResponse;
      assert.equal(getResponse.notebookStatus, 'Snapshot in progress');
      snapshotResponse = (await snapshotPromise).data as SnapshotResponse;
    }).timeout(600000);
    it('should return to status "Active"', async () => {
      const getResponse = (
        await axios.get(`${BASEURL}/notebook/${notebookResponse.id}`)
      ).data as GetNotebookResponse;
      assert.equal(getResponse.notebookStatus, 'Active');
    });

    it('should return github url', () =>
      assert.match(
        snapshotResponse.snapshotURL,
        /^https:\/\/github.com\/.+\/created-by-smoke-[0-9a-f]{8}$/
      ));

    it('should have succeeded pod', async () => {
      const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
      const podResponse = await k8sApi.listNamespacedPod(NAMESPACE);
      pod = podResponse.body.items.find(
        (pod) =>
          pod &&
          pod.metadata &&
          pod.metadata.labels &&
          pod.metadata.labels['cssservice-id'] === notebookResponse.id
      );
      assert.equal(pod && pod.status && pod.status.phase, 'Succeeded');
    });

    it('should be reachable', async () => {
      const response = await axios.get(snapshotResponse.snapshotURL);
      assert.exists(response);
    });
    it('should have required content', async () => {
      contents = (
        await axios.get(
          `${GITHUBAPI}/repos/${snapshotResponse.snapshotURL.substring(
            19
          )}/contents`,
          { headers: { Accept: 'application/vnd.github.v3+json' } }
        )
      ).data as Contents;
      expectedContents.forEach((entry) => {
        assert(
          contents.some((e) => e.name === entry.name && e.type === entry.type),
          `${entry.type} ${entry.name} not found in repo.`
        );
      });
    });
    it('should have requested libraries in environment.yml', async () => {
      const environmentRes = contents.find(
        (entry) => entry.name === 'environment.yml'
      );
      if (environmentRes) {
        const environmentYml = (await axios.get(environmentRes.download_url))
          .data as string;
        const environment: Environment = YAML.parse(
          environmentYml
        ) as Environment;
        let pip: string[] = [];
        const conda: string[] = [];
        environment.dependencies.forEach((dep) => {
          if (typeof dep === 'string') {
            conda.push(dep);
          } else {
            pip = dep.pip;
          }
        });
        const dependencies = conda
          .map((dep) => dep.split('='))
          .concat(pip.map((dep) => dep.split('==')));
        if (notebookResponse.userRequestedLibraries) {
          notebookResponse.userRequestedLibraries.forEach((requestedLib) => {
            const dependency = dependencies.find(
              (dep) => dep[0] === requestedLib.libname
            );
            assert.exists(dependency);
            if (requestedLib.libversion) {
              assert.include(
                dependency && dependency[1],
                requestedLib.libversion
              );
            }
          });
        } else {
          throw Error('No user requested libraries.');
        }
      } else {
        throw Error('No environment file found.');
      }
    });

    it('should generate provenance for the snapshot activity', async () => {
      let activityList = null;
      for (let i = 0; i < 120; i += 5) {
        try {
          activityList = await findActivityIdByType(
            notebookResponse.sessionId,
            ACTIVITIES.SNAPSHOT
          );
          assert.isNotEmpty(activityList);
          break;
        } catch (err) {
          await delay(5000);
        }
      }
      if (activityList === null) {
        throw Error('No snapshot activity was made');
      }
      const activityResponse = await getActivityById(activityList[0]['@id']);
      assert.isNotEmpty(activityResponse);
    }).timeout(125000);

    after(async () => {
      await axios.delete(
        `${GITHUBAPI}/repos/${snapshotResponse.snapshotURL.substring(19)}`,
        {
          headers: {
            Accept: 'application/vnd.github.v3+json',
            Authorization: `Bearer ${
              process.env.SWIRRL_API_GITHUB_APIKEY_TESTS as string
            }`,
          },
        }
      );
    });
  });
