import 'mocha';
import axios from 'axios';
import { HOSTNAME } from './globals';
import { GetNotebookResponse } from './types';

export const getNotebook = async (
  sessionId: string
): Promise<GetNotebookResponse> =>
  (await axios.get(`${HOSTNAME}/swirrl-api/v1.0/notebook/${sessionId}`))
    .data as GetNotebookResponse;
