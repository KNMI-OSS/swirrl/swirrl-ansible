import 'mocha';
import axios from 'axios';
import { HOSTNAME } from './globals';
import { ActivityList, Activity, Graph } from './types';

export const getActivities = async (sessionId: string): Promise<ActivityList> =>
  (
    await axios.get(
      `${HOSTNAME}/swirrl-api/v1.0/provenance/session/${sessionId}/activities`
    )
  ).data as ActivityList;

export const findActivityIdByType = async (
  sessionId: string,
  type: Activity
): Promise<Graph[]> => {
  const activities: ActivityList = await getActivities(sessionId);

  try {
    const target = activities['@graph'].filter((activity) =>
      activity['@type'].includes(type)
    );

    return target;
  } catch (error) {
    console.error(error);
    return [];
  }
};

export const getActivityById = async (
  activityId: string
): Promise<ActivityList> =>
  (
    await axios.get(
      `${HOSTNAME}/swirrl-api/v1.0/provenance/activity/${activityId}`
    )
  ).data as ActivityList;
