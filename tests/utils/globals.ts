export const HOSTNAME = process.env.HOSTNAME || 'http://localhost:8080';
export const BASEURL = `${HOSTNAME}/swirrl-api/v1.0`;
export const NAMESPACE = process.env.SWIRRL_API_NAMESPACE || 'swirrl';
export const GITHUBAPI = 'https://api.github.com';

export const delay = (t: number): Promise<void> =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, t);
  });
