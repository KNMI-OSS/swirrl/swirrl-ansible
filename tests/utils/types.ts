export type UserRequestedLibrary = {
  libname: string;
  libversion: string | null;
  source: string | null;
};

export type PostNotebookResponse = {
  id: string;
  poolId: string;
  serviceURL: string;
  sessionId: string;
  userRequestedLibraries: UserRequestedLibrary[] | null;
};

export type GetNotebookResponse = {
  notebookStatus: string;
  volumes: {
    name: string;
    size: number;
  }[];
  installedLibraries: UserRequestedLibrary[];
} & PostNotebookResponse;

export enum ACTIVITIES {
  CREATE = 'swirrl:CreateNotebook',
  RESTORE = 'swirrl:RestoreLibs',
  SNAPSHOT = 'swirrl:CreateSnapshot',
  UPDATE = 'swirrl:UpdateLibs',
  WORKFLOW = 'swirrl:RunWorkflow',
}

export type Activity =
  | ACTIVITIES.CREATE
  | ACTIVITIES.UPDATE
  | ACTIVITIES.RESTORE
  | ACTIVITIES.SNAPSHOT
  | ACTIVITIES.WORKFLOW;

export type Graph = {
  'swirrl:serviceId': string;
  'swirrl:sessionId': string;
  '@type': string[];
  person: string;
  '@id': string;
  'prov:endedAtTime': string;
  'prov:startedAtTime': string;
};

export type ActivityList = {
  '@graph': Graph[];
  '@context': Record<string, string>;
};
