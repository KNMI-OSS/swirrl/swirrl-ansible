#!/bin/sh

image=$1
tag=$2
branch=$3

exitcode=0

echo "Image: ${image}"
echo "Tag: ${tag}"
echo "Branch (slugged): ${branch}"

if docker pull ${image}:${tag} ; then
  ## This commit triggered a build, tag it with the current branch.
  echo "Tagging ${image}:${branch}"
  docker tag ${image}:${tag} ${image}:${branch}
  status=$?
  let exitcode=$((exitcode+status))
  echo "Pushing ${image}:${branch}"
  docker push ${image}:${branch}
  status=$?
  let exitcode=$((exitcode+status))
else
  # If the current commit didn't trigger a build, pull the latest from
  # the branch and tag it with this commit hash.
  echo "Pulling ${image}:${branch}"
  docker pull ${image}:${branch}
  status=$?
  let exitcode=$((exitcode+status))
  echo "Tagging ${image}:${tag}"
  docker tag ${image}:${branch} ${image}:${tag}
  status=$?
  let exitcode=$((exitcode+status))
  echo "Pushing ${image}:${tag}"
  docker push ${image}:${tag}
  status=$?
  let exitcode=$((exitcode+status))
fi
exit ${exitcode}