FROM node:16-bullseye-slim

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

# Install dependencies
COPY package.json ./package.json
COPY yarn.lock ./yarn.lock
COPY tsconfig.json ./tsconfig.json
RUN yarn --prod --frozen-lockfile
## TODO Re-enable this after migrating to kubernetes that uses fetch.
## See https://github.com/kubernetes-client/javascript/blob/HEAD/FETCH_MIGRATION.md
## RUN yarn audit

COPY tests ./tests

ENV HOSTNAME="http://swirrl-api"

CMD [ "yarn", "test" ]
